//
//  AppDelegate.swift
//  CallerAppTest
//
//  Created by Rafay Misfit on 14/6/20.
//  Copyright © 2020 Misfit. All rights reserved.
//

import UIKit
import CallerSDK
import JitsiMeet
import PushKit
import CallKit
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKPushRegistryDelegate {
    
    var window: UIWindow?
    var voipRegistory: PKPushRegistry?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if let isRegistered = UserDefaults.standard.value(forKey: "userRegistered") as? Bool, isRegistered {
            let userlistScreen = UserListViewController()
            let navigationVc = UINavigationController.init(rootViewController: userlistScreen)
            self.window?.rootViewController = navigationVc
        } else {
            let registrationView = RegistrationViewController()
            self.window?.rootViewController = registrationView
        }
        
        App2AppManager.shared.connectSocket()
        App2AppManager.shared.connectCallManager()
        self.voipRegistration()
        IQKeyboardManager.shared.enable = true
        
        self.window?.makeKeyAndVisible()
        guard let launchOptions = launchOptions else { return false }
        return JitsiMeet.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        App2AppManager.shared.disconnectSocket()
    }
    
    func voipRegistration() {
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: nil)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return JitsiMeet.sharedInstance().application(application, continue: userActivity, restorationHandler: restorationHandler)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return JitsiMeet.sharedInstance().application(app, open: url, options: options)
    }    
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let tokenParts = pushCredentials.token.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        if type == .voIP {
            UserDefaults.standard.setValue(token, forKey: "voipToken")
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        if let data = payload.dictionaryPayload as? [String: AnyObject], let callerId = data["call"] as? String, let callerPhone = data["callerPhone"] as? String {
            App2AppManager.shared.reportNewIncomingCall(uuid: UUID(), handle: callerPhone, contactIdentifier: callerPhone) { (error) in
                completion()
                App2AppManager.shared.receiveCallFor(callerId: callerId, callerPhoneNumber: callerPhone)
            }
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        debugPrint("Invalid")
    }
    
}
