//
//  UserListViewController.swift
//  JitsiCaller
//
//  Created by Rafay Misfit on 3/6/20.
//  Copyright © 2020 Misfit. All rights reserved.
//

import UIKit
import CallerSDK

class UserListViewController: UIViewController {

    var syncedContactList = [[String:String]]()
    let userListTableCellIdentifier = "UserListTableCellIdentifier"
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        App2AppManager.shared.delegate = self
        self.title = "App contacts"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sync", style: .plain, target: self, action: #selector(contactSyncAction))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Call History", style: .plain, target: self, action: #selector(callHistoryAction))
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UserListTableViewCell.nib(), forCellReuseIdentifier: userListTableCellIdentifier)
        App2AppManager.shared.getSyncedContactList()
    }
    
    @objc func contactSyncAction() {
        self.syncedContactList.removeAll()
        App2AppManager.shared.getSyncedContactList()
    }
    
    @objc func callHistoryAction() {
        App2AppManager.shared.getCallHistory()
    }
    
    @objc func audioButtonAction(sender:UIButton)
    {
        let obj = self.syncedContactList[sender.tag]
        guard let name = obj["name"], let userId =  obj["phone"] else {
            return
        }
        App2AppManager.shared.initiateCallFor(userName: name, withUserId: userId, callType: "audio")
    }
    
    @objc func videoButtonAction(sender:UIButton)
    {
        let obj = self.syncedContactList[sender.tag]
        guard let name = obj["name"], let userId =  obj["phone"] else {
            return
        }
        App2AppManager.shared.initiateCallFor(userName: name, withUserId: userId, callType: "video")
    }
}

extension UserListViewController: UITableViewDataSource {
    
    private func syncedWithJitsi(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: userListTableCellIdentifier) as! UserListTableViewCell
        
        let obj = self.syncedContactList[indexPath.row]
        cell.userNameLabel.text = obj["name"]
        cell.userPhoneLabel.text = obj["phone"]
        cell.audioButton.tag = indexPath.row
        cell.videoButton.tag = indexPath.row
        cell.videoButton.addTarget(self, action: #selector(videoButtonAction), for: .touchUpInside)
        cell.audioButton.addTarget(self, action: #selector(audioButtonAction), for: .touchUpInside)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.syncedContactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return syncedWithJitsi(tableView, cellForRowAt: indexPath)
    }
}

extension UserListViewController: App2AppManagerDelegate {
    
    func socketConnected() {
        debugPrint("socket connected")
    }
    
    func getContactPermissionDeclined() {
        debugPrint("permission declined")
    }
    
    func contactSyncDidSuccess() {
        debugPrint("Contact sync success")
    }
    
    func contactSyncingFailedWith(errorCode: Int) {
        debugPrint(errorCode)
    }
    
    func fetchedchedContactList(contactList: [[String:String]]) {
        debugPrint(contactList)
        self.syncedContactList = contactList
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func fetchingContactListFailedWith(errorCode: Int) {
        debugPrint(errorCode)
    }
    
    func joinedCall() {
        debugPrint("call received")
    }
    
    func endedCall() {
        debugPrint("ended call")
    }
    
    func callHistoryFetchFailedWith(errorCode: Int) {
        debugPrint(errorCode)
    }
    
    func callHistoryFetchSuccessWith(history: [[String : Any]]) {
        debugPrint(history)
    }
}

