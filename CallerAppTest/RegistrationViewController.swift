//
//  RegistrationViewController.swift
//  JitsiCaller
//
//  Created by Rafay Misfit on 8/6/20.
//  Copyright © 2020 Misfit. All rights reserved.
//

import UIKit
import CallerSDK

class RegistrationViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var userIdTextField: UITextField!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicatorView.isHidden = true
        // Do any additional setup after loading the view.
    }

    @IBAction func submitAction(_ sender: Any) {
        
        if let fullName = self.nameTextField.text, !fullName.isEmpty, let phoneNumber = self.userIdTextField.text, !phoneNumber.isEmpty {
            let result = phoneNumber.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
            let app2appcallManager = App2AppManager()
            app2appcallManager.delegate = self
            app2appcallManager.registerUserWith(userName: fullName, phoneNumber: result)
            self.activityIndicatorView.startAnimating()
            self.activityIndicatorView.isHidden = false
        }
    }
}

extension RegistrationViewController: App2AppManagerDelegate {
    func userRegistrationDidSuccess() {
        debugPrint("registration Success")
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true
        let userlistScreen = UserListViewController()
        let navigationVc = UINavigationController.init(rootViewController: userlistScreen)
        UIApplication.shared.keyWindow?.rootViewController = navigationVc
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    func userRegistrationFailedWith(errorCode: Int) {
        debugPrint(errorCode)
        let message = "Registration  failed with error code \(errorCode)"
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    }
}
